#!/bin/sh

# terminate running instance
killall -q ulauncher

# wait
while pgrep -u $USER -x ulauncher >/dev/null; do sleep 1; done

ulauncher --no-window-shadow --hide-window &