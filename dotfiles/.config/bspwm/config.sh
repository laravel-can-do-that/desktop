#!/bin/sh

bspc config window_gap 20
bspc config split_ratio 0.50
bspc config border_width 2

bspc config focus_follows_pointer true
bspc config pointer_follows_focus true
bspc config point_follows_monitor true