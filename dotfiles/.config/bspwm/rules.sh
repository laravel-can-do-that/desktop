#! /bin/sh

desktop bspwm --instance=$3 --class=$2 --id=$1 --consequences="$4" &
