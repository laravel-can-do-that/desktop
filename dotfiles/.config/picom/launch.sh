#!/bin/sh

# terminate running instance
killall -q picom

while pgrep -u $USER -x picom >/dev/null; do sleep 1; done
picom &
