#!/bin/sh

# terminate running instance
killall -q xsettingsd

# ensure always running
while :; do
    if pgrep -x xsettingsd > /dev/null; then
        true
    else
        xsettingsd &
    fi
    sleep 5
done &