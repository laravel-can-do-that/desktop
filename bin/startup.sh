#!/bin/sh
# Would be great to get this entire sequence into something like php desktop startup
clear

xrdb -merge ~/.Xresources

# fixes
xsetroot -cursor_name left_ptr

# launch
# Does this solve the appindicator issue?
# https://askubuntu.com/questions/1097737/gnome-panel-applet-indicator-applet-complete-is-missing-icons
exec gnome-flashback > "$HOME/startup.log" &
exec /usr/libexec/gsd-xsettings > "$HOME/startup.log" &
exec /usr/libexec/polkit-gnome-authentication-agent-1 > "$HOME/startup.log" &
#exec /usr/bin/gnome-screensaver
# xsettingsd-autostart > "$HOME/startup.log" &

# https://github.com/regolith-linux/regolith-desktop/issues/364#issuecomment-622456865
gsettings set org.gnome.gnome-flashback status-notifier-watcher false

"$HOME/.config/sxhkd/launch.sh" > "$HOME/startup.log" &
"$HOME/.config/picom/launch.sh" > "$HOME/startup.log" &
"$HOME/.config/polybar/launch.sh" > "$HOME/startup.log" &
"$HOME/.config/dunst/launch.sh" > "$HOME/startup.log" &
# gnome-flashback seems to take care of this as well as a lot of other things like natural scrolling, do we need the xsettingsd-autostart now?
#"$HOME/.config/feh/launch.sh" > "$HOME/.config/feh/log" &
#"$HOME/.config/ulauncher/launch.sh" > "$HOME/.config/ulauncher/log" &
