<?php

namespace OperatingSystems\Sockets;

class Socket
{
    protected \Socket $socket;

    protected bool $connected = false;

    protected string $address;

    protected int|null $port = null;

    protected int $domain;

    protected int $type;

    protected int $protocol;

    protected string $response;

    public function domain(int $value): static
    {
        return tap($this, fn () => $this->domain = $value);
    }

    public function type(int $value): static
    {
        return tap($this, fn () => $this->type = $value);
    }

    public function protocol(int $value): static
    {
        return tap($this, fn () => $this->protocol = $value);
    }

    public function address(string $value): static
    {
        return tap($this, fn () => $this->address = $value);
    }

    public function port(int $value): static
    {
        return tap($this, fn () => $this->port = $value);
    }

    public function unix(): static
    {
        return $this->domain(AF_UNIX);
    }

    public function ipv4(): static
    {
        return $this->domain(AF_INET);
    }

    public function ipv6(): static
    {
        return $this->domain(AF_INET6);
    }

    public function stream(): static
    {
        return $this->type(SOCK_STREAM);
    }

    public function connect(): static
    {
        if($this->isNotConnected()) {
            $this->connected = socket_connect($this->create(), $this->address, $this->port);
        }

        return $this;
    }

    public function isConnected(): bool
    {
        return $this->connected;
    }

    public function isNotConnected(): bool
    {
        return ! $this->isConnected();
    }

    protected function create(): \Socket
    {
        return $this->socket = isset($this->socket) ? $this->socket : socket_create(
            $this->domain,
            $this->type,
            $this->protocol
        );
    }

    public function write(string $message): static
    {
        $this->connect();

        socket_write($this->socket, $message);

        return $this;
    }

    public function read(int $bytes = 1024): string
    {
        return $this->response = isset($this->response) ? $this->response : socket_read($this->socket, $bytes);
    }

    public function close()
    {
        socket_close($this->socket);
        unset($this->socket);

        return $this;
    }
}
