<?php

use OperatingSystems\Facades\OperatingSystem;

if (! function_exists('home_path')) {
    function home_path($path = '')
    {
        return OperatingSystem::homePath($path);
    }
}

if (! function_exists('dotfiles_path')) {
    function dotfiles_path($path = '')
    {
        return config('filesystems.disks.dotfiles.root').($path ? DIRECTORY_SEPARATOR.ltrim($path, DIRECTORY_SEPARATOR) : $path);
    }
}
