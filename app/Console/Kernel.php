<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use OperatingSystems\Commands\Brightness;
use OperatingSystems\Commands\Monitors;
use OperatingSystems\Commands\DateTime\DateTime;
use OperatingSystems\Commands\Packages;
use OperatingSystems\Commands\Release;
use OperatingSystems\Commands\Terminal;
use OperatingSystems\Commands\Volume;
use OperatingSystems\Packages\Alacritty\Console\Commands\Alacritty;
use OperatingSystems\Packages\AlsaUtils\Console\Commands\Amixer;
use OperatingSystems\Packages\Apt\Console\Commands as Apt;
use OperatingSystems\Packages\AptCache\Console\Commands as AptCache;
use OperatingSystems\Packages\BrightnessCtl\Console\Commands as BrightnessCtl;
use OperatingSystems\Packages\Bspwm\Console\Commands\Bspc;
use OperatingSystems\Packages\Dnf\Console\Commands as Dnf;
use OperatingSystems\Packages\Flatpak\Console\Commands as Flatpak;
use OperatingSystems\Packages\Git\Console\Commands as Git;
use OperatingSystems\Packages\LibNotify\Console\Commands as LibNotify;
use OperatingSystems\Packages\LinuxStandardBase\Console\Commands\LinuxStandardBase;
use OperatingSystems\Packages\Rofi\Console\Commands as Rofi;
use OperatingSystems\Packages\Snap\Console\Commands as Snap;
use OperatingSystems\Packages\Sxhkd\Console\Commands\Sxhkd;
use OperatingSystems\Packages\Ulauncher\Console\Commands\Ulauncher;
use OperatingSystems\Packages\Xrandr\Console\Commands\Xrandr;

class Kernel extends ConsoleKernel
{
    protected $commands = [
        Xrandr::class,
        Xrandr\Monitors::class,
        Xrandr\Monitors\Primary::class,
        DateTime::class,
        LinuxStandardBase::class,
        Dnf\Install::class,
        Dnf\Clean::class,
        Dnf\AutoRemove::class,
        Dnf\MakeCache::class,
        Dnf\Update::class,
        Apt\Install::class,
        Apt\Upgrade::class,
        Apt\AutoRemove::class,
        Apt\AutoClean::class,
        AptCache\Policy::class,
        Packages\Make::class,
        Packages\Candidates\Make::class,
        LibNotify\NotifySend::class,
        Release::class,
        Terminal::class,
        Rofi\Rofi::class,
        Bspc\Query::class,
        Bspc\Desktop::class,
        Bspc\Node::class,
        Alacritty::class,
        Rofi\Rofi\Window::class,
        Rofi\Rofi\Drun::class,
        Ulauncher\Toggle::class,
        Brightness\Get::class,
        Brightness\Up::class,
        Brightness\Down::class,
        Monitors\Primary::class,
        Volume\Get::class,
        Volume\Mute::class,
        Volume\Unmute::class,
        Volume\Up::class,
        Volume\Down::class,
        Volume\Toggle::class,
        Amixer\Get::class,
        Amixer\Mute::class,
        Amixer\Unmute::class,
        Amixer\Up::class,
        Amixer\Down::class,
        Amixer\Toggle::class,
        BrightnessCtl\Get::class,
        BrightnessCtl\Max::class,
        BrightnessCtl\Up::class,
        BrightnessCtl\Down::class,
        Git\Replicate::class,
        Git\Pull::class,
        Git\Fetch::class,
        Snap\Install::class,
        Snap\Installed::class,
        Snap\Remove::class,
        Snap\Refresh::class,
        Flatpak\Install::class,
        Flatpak\Update::class,
        Flatpak\Repository\Add::class,
        Sxhkd::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
