<?php

namespace App\Console\Commands\Theme;

use Illuminate\Console\Command;

class ListThemes extends Command
{
    protected $signature = 'theme:list';

    protected $description = 'List the available themes for your desktop.';

    public function handle()
    {
        $this->table(
            ['Name', 'Colors'],
            ['Nord', 'Blue']
        );
    }
}
