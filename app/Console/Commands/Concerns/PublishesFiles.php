<?php

namespace App\Console\Commands\Concerns;

use Illuminate\Support\Facades\File;

trait PublishesFiles
{
    protected function publish(): void
    {
        $this->createDirectories();

        $this->copyFiles();

        $this->createLinks();
    }

    protected function copyFiles(): void
    {
        $files = collect(method_exists($this, 'files') ? $this->directories() : []);

        $files->each(
            fn ($target, $source) => File::copy($source, $target)
        );
    }

    protected function createDirectories(): void
    {
        $directories = collect(method_exists($this, 'directories') ? $this->directories() : []);

        $directories->reject(
            fn ($path) => File::exists($path)
        )->each(
            fn ($path) => File::makeDirectory(path: $path, recursive: true)
        );
    }

    protected function isRemovableSymlink(string $link, bool $force): bool
    {
        return is_link($link) && $force;
    }

    protected function createLinks(): void
    {
        $links = collect(method_exists($this, 'links') ? $this->links() : []);

        if ($links->isEmpty()) {
            return;
        }

        $links->each(function ($target, $link) {
            if (file_exists($link) && ! $this->isRemovableSymlink($link, $this->option('force'))) {
                return;
            }

            if (is_link($link)) {
                $this->laravel->make('files')->delete($link);
            }

            $this->laravel->make('files')->link($target, $link);
        });
    }
}
