<?php

namespace App\Console\Commands\Packages;

use Illuminate\Console\Command;
use OperatingSystems\Commands\Snap;

class Sanitize extends Command
{
    protected $signature = 'packages:sanitize';

    protected $description = 'Sanitize packages pre-installed by operating system.';

    public function handle()
    {
        $this->call(Snap\Remove::class, ['packages' => ['firefox', 'snap-store'], '--purge' => true]);
    }
}
