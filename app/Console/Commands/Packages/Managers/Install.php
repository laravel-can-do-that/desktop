<?php

namespace App\Console\Commands\Packages\Managers;

use Illuminate\Console\Command;
use OperatingSystems\Facades\OperatingSystem;
use OperatingSystems\Packages\Contracts\Package;
use OperatingSystems\Packages\Flatpak\Console\Commands\Repository\Add;
use OperatingSystems\Packages\Flatpak\Flatpak;
use OperatingSystems\Packages\Snap\Snap;

class Install extends Command
{
    protected $signature = 'packages:managers:install';

    protected $description = 'Install package managers.';

    public function handle()
    {
        OperatingSystem::packageManagers()->reject->isDefault()->each(function ($packageManager) {
            match ($packageManager::class) {
                Snap::class => $this->installSnap(),
                Flatpak::class => $this->installFlatpak()
            };
        });
    }

    protected function installFlatpak()
    {
        $flatpak = resolve(Flatpak::class);

        $this->task('Install Flatpak', fn () => $this->installPackage($flatpak));

        // How can we get this to happen automatically as part of package installation?
        $this->call(
            Add::class,
            [
                'name' => $flatpak->defaultRepository()->name(),
                'location' => $flatpak->defaultRepository()->url(),
                '--if-not-exists' => true,
            ]
        );
    }

    protected function installSnap()
    {
        $snap = resolve(Snap::class);

        $this->task('Install Snap', fn () => $this->installPackage($snap));
    }

    protected function installPackage(Package $package)
    {
        return $package->preferredManager()->install($package);
    }
}
