<?php

namespace App\Console\Commands\Packages;

use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use OperatingSystems\Packages\Alacritty\Alacritty;
use OperatingSystems\Packages\AlsaUtils\AlsaUtils;
use OperatingSystems\Packages\AsciiArtLibrary\AsciiArtLibrary;
use OperatingSystems\Packages\AsciiQuarium\AsciiQuarium;
use OperatingSystems\Packages\Bpytop\Bpytop;
use OperatingSystems\Packages\BreezeCursorTheme\BreezeCursorTheme;
use OperatingSystems\Packages\Bspwm\Bspwm;
use OperatingSystems\Packages\Cava\Cava;
use OperatingSystems\Packages\Code\Code;
use OperatingSystems\Packages\Contracts\Package;
use OperatingSystems\Packages\Curl\Curl;
use OperatingSystems\Packages\Docker\Docker;
use OperatingSystems\Packages\Dunst\Dunst;
use OperatingSystems\Packages\Feh\Feh;
use OperatingSystems\Packages\FiraCode\FiraCode;
use OperatingSystems\Packages\Firefox\Firefox;
use OperatingSystems\Packages\Flatseal\Flatseal;
use OperatingSystems\Packages\FontAwesome\FontAwesome;
use OperatingSystems\Packages\FontManager\FontManager;
use OperatingSystems\Packages\GnomeSoftware\GnomeSoftware;
use OperatingSystems\Packages\GnomeTweaks\GnomeTweaks;
use OperatingSystems\Packages\Make\Make;
use OperatingSystems\Packages\MateMedia\MateMedia;
use OperatingSystems\Packages\MateScreenshot\MateScreenshot;
use OperatingSystems\Packages\MateUtils\MateUtils;
use OperatingSystems\Packages\MenuLibre\MenuLibre;
use OperatingSystems\Packages\Neofetch\Neofetch;
use OperatingSystems\Packages\Nfs\Nfs;
use OperatingSystems\Packages\NotoColorEmoji\NotoColorEmoji;
use OperatingSystems\Packages\PapirusIconTheme\PapirusIconTheme;
use OperatingSystems\Packages\Peek\Peek;
use OperatingSystems\Packages\Picom\Picom;
use OperatingSystems\Packages\Polybar\Polybar;
use OperatingSystems\Packages\RedShift\RedShift;
use OperatingSystems\Packages\Rofi\Rofi;
use OperatingSystems\Packages\Slack\Slack;
use OperatingSystems\Packages\Spotify\Spotify;
use OperatingSystems\Packages\Ssh\Ssh;
use OperatingSystems\Packages\Sxhkd\Sxhkd;
use OperatingSystems\Packages\Telegram\Telegram;
use OperatingSystems\Packages\Ulauncher\Ulauncher;
use OperatingSystems\Packages\Unimatrix\Unimatrix;
use OperatingSystems\Packages\UsbCreatorGtk\UsbCreatorGtk;
use OperatingSystems\Packages\Wget\Wget;
use OperatingSystems\Packages\Zoom\Zoom;
use OperatingSystems\Packages\Zsh\Zsh;

class Install extends Command
{
    protected $signature = 'packages:install';

    protected $description = 'Install packages.';

    protected $packages = [
        // Liquorix?
        AlsaUtils::class,
        Alacritty::class,
        AsciiArtLibrary::class,
        AsciiQuarium::class,
        Bpytop::class,
        BreezeCursorTheme::class,
        Bspwm::class,
        Cava::class,
        Code::class,
        Curl::class,
        Docker::class,
        Dunst::class,
        Feh::class,
        FiraCode::class,
        Firefox::class,
        Flatseal::class,
        FontAwesome::class,
        FontManager::class,
        GnomeSoftware::class,
        GnomeTweaks::class,
        Make::class,
        // MateMedia::class,
        MateScreenshot::class,
        // MateUtils::class,
        // MenuLibre::class,
        Neofetch::class,
        Nfs::class,
        NotoColorEmoji::class,
        PapirusIconTheme::class,
        Peek::class,
        Picom::class,
        Polybar::class,
        RedShift::class,
        Rofi::class,
        Slack::class,
        Spotify::class,
        Ssh::class,
        Sxhkd::class,
        Telegram::class,
        Ulauncher::class,
        // Unimatrix::class,
        // UsbCreatorGtk::class,
        Wget::class,
        Zoom::class,
        Zsh::class,
    ];

    public function handle()
    {
        // Get Collection of Packages to be installed
        // This should be filtered by the .env/config so that installation can be enabled/disabled per Package
        // This Collection should also match each Package to the configured PackageManager to use for installation to allow user customization
        $this->packages()->each(
            fn (Package $package) => $this->task(
                $this->getTask($package), fn () => $this->installPackage($package)
            )
        );
    }

    protected function packages(): Collection
    {
        return collect($this->packages)->map(fn ($class) => new $class());
    }

    protected function getTask(Package $package): string
    {
        return sprintf('Install %s', $package->name());
    }

    protected function installPackage(Package $package)
    {
        return $package->preferredManager()->install($package);
    }
}
