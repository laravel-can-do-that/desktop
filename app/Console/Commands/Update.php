<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Update extends Command
{
    protected $signature = 'update {--y|yes}';

    protected $description = 'Update your desktop.';

    public function title(string $title): Command
    {
        $size = strlen($title);
        $spaces = str_repeat(' ', $size);

        $this->output->newLine();
        $this->output->writeln("<bg=blue;fg=white>$spaces$spaces$spaces</>");
        $this->output->writeln("<bg=blue;fg=white>$spaces$title$spaces</>");
        $this->output->writeln("<bg=blue;fg=white>$spaces$spaces$spaces</>");
        $this->output->newLine();

        return $this;
    }

    public function handle()
    {
        if (! $this->confirmed()) {
            return;
        }

        $this->captureRoot();
        $this->runOperatingSystemTasks();
    }

    protected function confirmed(): bool
    {
        return $this->option('yes') || $this->confirm('Root privileges are required to install packages, would you like to continue?');
    }

    protected function captureRoot(): void
    {
        $this->call(CaptureRoot::class);
    }

    protected function runOperatingSystemTasks(): void
    {
        $this->title('Operating System');

        $this->call(Os\Update::class);
    }
}
