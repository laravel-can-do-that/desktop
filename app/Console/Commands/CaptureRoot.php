<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;

class CaptureRoot extends Command
{
    protected $signature = 'root:capture';

    protected $description = 'Capture root privileges.';

    public function handle()
    {
        (new Process(['sudo', 'ls', '-la']))->setTty(false)->run();
    }
}
