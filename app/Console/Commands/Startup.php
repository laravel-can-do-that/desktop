<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Startup extends Command
{
    protected $signature = 'startup';

    protected $description = 'Start up desktop.';

    public function handle()
    {
        $this->call(Sxhkd::class);
        $this->call(Picom::class);
        $this->call(Polybar::class);
    }
}
