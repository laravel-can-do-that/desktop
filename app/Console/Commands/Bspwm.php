<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class Bspwm extends Command
{
    protected $signature = 'bspwm {--class=} {--instance=} {--id=} {--consequences=}';

    protected $description = 'BSPWM';

    public function handle()
    {
        match ($this->option('instance')) {
            'Alacritty' => $this->alacritty(),
            'code', 'Code' => $this->code(),
            'gnome-control-center' => $this->settings(),
            'slack', 'Slack' => $this->slack(),
            'telegram-desktop' => $this->telegram(),
            'ulauncher', 'Ulauncher' => $this->ulauncher(),
            'zoom.real', 'zoom', 'Zoom' => $this->zoom(),
            default => $this->fallback()
        };
    }

    private function windowNames(): Collection
    {
        return Str::of(shell_exec(
            sprintf('xprop -id %s WM_CLASS', $this->option('id'))
        ))->after('=')->trim()->explode(',')->map(
            fn ($name) => (string) Str::of($name)->trim()->replace('"', '')
        );
    }

    public function fallback()
    {
        // TODO: Maybe this approach makes more sense with xdo? Apparently xdotool can be used in place of xdo?
        // https://www.reddit.com/r/bspwm/comments/qx0qrh/bspc_rules_arent_applied/
        // https://github.com/tiagovla/.dotfiles/blob/197424e72c358016b6770c9b22462af642dee99f/bspwm/.config/bspwm/external_rules#L124-L127

        $names = $this->windowNames();

        // match (true) {
        //     $names->contains('spotify') => $this->spotify(),
        //     default => null
        // };

        if ($names->contains('spotify')) {
            $this->spotify();
        }
    }

    public function alacritty()
    {
        $this->line('desktop=^4');
        $this->line('follow=on');
    }

    public function code()
    {
        $this->line('desktop=^3');
        $this->line('follow=on');
    }

    public function settings()
    {
        $this->line('desktop=^11');
        $this->line('follow=on');
    }

    public function slack()
    {
        $this->line('desktop=^2');
        $this->line('follow=on');
    }

    public function spotify()
    {
        $this->line('desktop=^10');
        $this->line('follow=on');
    }

    public function telegram()
    {
        $this->line('desktop=^5');
        $this->line('follow=on');
    }

    public function ulauncher()
    {
        $this->line('border=off');
    }

    public function zoom()
    {
        $this->line('state=floating');
        $this->line('desktop=share');
        $this->line('follow=on');
    }
}
