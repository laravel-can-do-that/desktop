<?php

namespace App\Console\Commands\Wallpaper;

use App\Console\Commands\Concerns\PublishesFiles;
use Illuminate\Console\Command;

class Set extends Command
{
    use PublishesFiles;

    protected $signature = 'wallpaper:set';

    protected $description = 'Set the theme wallpaper';

    public function handle()
    {
        $this->task('Set wallpaper', function () {
            $this->publish();
        });
    }

    protected function directories(): array
    {
        return [
            home_path('Photos/wallpapers'),
        ];
    }

    protected function links(): array
    {
        // collect(File::allFiles(base_path('assets/wallpapers')))->dd();
    }
}
