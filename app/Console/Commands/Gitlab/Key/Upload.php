<?php

namespace App\Console\Commands\Gitlab\Key;

use App\Console\Commands\Gitlab\Token;
use GrahamCampbell\GitLab\Facades\GitLab;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;

class Upload extends Command
{
    protected $signature = 'gitlab:key:upload {--y|yes}';

    protected $description = 'Upload SSH Key to Gitlab';

    public function handle()
    {
        $this->call(Token\Set::class, ['-y' => $this->option('yes')]);

        $this->task('Upload SSH Key to Gitlab', function () {
            $this->upload();
        });
    }

    protected function upload(): void
    {
        if ($this->keyExists()) {
            return;
        }

        GitLab::users()->createKey(
            $this->hostname(),
            $this->key(),
        );
    }

    protected function keyExists(): bool
    {
        return (bool) $this->keys()->firstWhere('title', $this->hostname());
    }

    protected function hostname(): string
    {
        return gethostname();
    }

    protected function keys(): Collection
    {
        return collect(
            Gitlab::users()->keys()
        );
    }

    protected function key(): string
    {
        return File::get(
            home_path('.ssh/id_rsa.pub')
        );
    }
}
