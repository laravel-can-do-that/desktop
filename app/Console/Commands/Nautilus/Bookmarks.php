<?php

namespace App\Console\Commands\Nautilus;

use App\Console\Commands\Concerns\PublishesFiles;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;

class Bookmarks extends Command
{
    use PublishesFiles;

    protected $signature = 'nautilus:bookmarks';

    protected $description = 'Add bookmarks to nautilus.';

    public function handle()
    {
        $this->task('Nautilus bookmarks', function () {
            $this->addBookmarks();
        });
    }

    protected function addBookmarks(): void
    {
        File::put(
            $this->currentPath(),
            $this->updatedBookmarks()->join(PHP_EOL)
        );
    }

    protected function currentPath(): string
    {
        return home_path('.config/gtk-3.0/bookmarks');
    }

    protected function currentBookmarks(): Collection
    {
        return collect(
            explode(
                PHP_EOL,
                File::get($this->currentPath())
            )
        )->filter();
    }

    protected function additionsPath(): string
    {
        return home_path('.config/nautilus/bookmarks');
    }

    protected function additionalBookmarks(): Collection
    {
        $content = rescue(fn () => File::get($this->additionsPath()), '');

        return collect(
            explode(
                PHP_EOL,
                $content
            )
        );
    }

    protected function updatedBookmarks(): Collection
    {
        return $this->currentBookmarks()->merge(
            $this->additionalBookmarks()->reject(
                fn ($path) => $this->currentBookmarks()->contains($path)
            )
        );
    }
}
