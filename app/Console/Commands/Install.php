<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

// TODO: All themes should update the theme for all applications that the theme is available for
// TODO: GTK4 apps do not respect the selected theme, the themes' gtk4.0/gtk.css files needs to be copied to ~/.config/gtk4.0
// TODO: How to use Tailwind config for theme color
// TODO: Set sudo password prompt to print asterisks
// TODO: Is there a way to install the GTK theme into the location where the Light / Dark theme lives in vanilla gnome?
// TODO: disable desktop icons on gnome-flashback gsettings set org.gnome.gnome-flashback.desktop show-icons false
class Install extends Command
{
    protected $signature = 'install {--y|yes}';

    protected $description = 'Install your desktop.';

    public function handle()
    {
        if (!$this->confirmed()) {
            return;
        }

        // Install session files
        // 0644 permissions
        // sessions/bspwm-gnome-xsession.desktop => /usr/share/xsessions/bspwm-gnome.desktop
        // sessions/bspwm-gnome.desktop => /usr/share/applications/bspwm-gnome.desktop
        // sessions/bspwm-gnome.session => /usr/share/gnome-session/sessions/bspwm-gnome.session
        // sessions/bspwm-gnome => /usr/bin/bspwm-gnome
        // sessions/gnome-session-bspwm => /usr/bin/gnome-session-bspwm

        // TODO: Remaining tasks
        // Clone dotfiles repository?
        // Install / configure theme
        // Ulauncher, bpytop, polybar, gtk (flatpak + snap settings), dunst
        // user specific customizations like
        // $this->call(Nautilus\Bookmarks::class);
        // Override Ctrl+Shift+E so that unicode isn't trigger and VS Code is safe
        // TODO: Rebuild

        $this->captureRoot();
        $this->runOperatingSystemTasks();
        $this->runSshKeyTasks();
        $this->runPackageInstallationTasks();

        // $this->call(Flatpak\Install::class);
    }

    protected function confirmed(): bool
    {
        return $this->option('yes') || $this->confirm('Root privileges are required to install packages, would you like to continue?');
    }

    protected function captureRoot(): void
    {
        $this->call(CaptureRoot::class);
    }

    protected function runOperatingSystemTasks(): void
    {
        $this->title('Operating System');

        // TODO: How do we customize this command per OS?
        // $this->call(Sanitize::class);
        $this->call(Os\Update::class);
    }

    protected function runSshKeyTasks(): void
    {
        $this->title('       SSH      ');

        $this->call(Ssh\Key\Generate::class);
        $this->call(Github\Key\Upload::class, ['-y' => $this->option('yes')]);
        $this->call(Gitlab\Key\Upload::class, ['-y' => $this->option('yes')]);
    }

    protected function runPackageInstallationTasks()
    {
        $this->title('Package Managers');
        $this->call(Packages\Managers\Install::class);

        $this->title('    Packages    ');
        $this->call(Packages\Install::class);

        // TODO: This is temporary right now until Git is implemented as a Package Candidate
        // $this->call(InstallNerdFonts::class);
    }
}
