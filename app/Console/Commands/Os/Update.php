<?php

namespace App\Console\Commands\Os;

use App\Console\Commands\CaptureRoot;
use Illuminate\Console\Command;
use OperatingSystems\Packages\Dnf\Console\Commands as Dnf;
use OperatingSystems\Packages\Flatpak\Console\Commands as Flatpak;
use OperatingSystems\Packages\Snap\Console\Commands as Snap;

class Update extends Command
{
    protected $signature = 'os:update';

    protected $description = 'Update operating system.';

    public function handle()
    {
        // TODO: How do we detect the righ OS and commands to run?
        // TODO: Should this live in \OperatingSystems\Commands instead?
        // TODO: Build a desktop notification system into the operating-system package, that supports urgency levels
        $this->call(CaptureRoot::class);
        $this->call(Dnf\Clean::class);
        $this->call(Dnf\MakeCache::class);
        $this->call(Dnf\Update::class, ['-y' => true]);
        $this->call(Dnf\AutoRemove::class, ['-y' => true]);
        $this->call(Flatpak\Update::class, ['-y' => true]);
        // TODO: Snap install / refresh prompts for elevated permissions through if sudo is not used. How?
        $this->call(Snap\Refresh::class);
        // TODO: Snap needs a refresh command to run here too if it's installed
        // TODO: The firmware should be updated as well using fwupdmgr
    }
}
