<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use OperatingSystems\Packages\Git\Facades\Git;
use OperatingSystems\Packages\Git\Services\Repository;

// TODO: This is here as a placeholder as it was removed from install.sh, need to make Git a package candidate
class InstallNerdFonts extends Command
{
    protected $signature = 'install:nerd-fonts';

    protected $description = 'Install / update nerd fonts';

    protected $path = '.programs/fonts/nerd-fonts';

    protected $url = 'https://github.com/ryanoasis/nerd-fonts.git';

    protected Repository $repository;

    public function handle()
    {
        $this->repository = Git::open($this->path());
        File::exists($this->path()) ? $this->update() : $this->install();
    }

    private function install()
    {
        File::ensureDirectoryExists(dirname($this->path()));

        // TODO: How can we let the user specify a specific branch?
        $this->repository->clone($this->url, 1);
        $this->runScript();
    }

    private function update()
    {
        // TODO: Hard reset changes
        $this->repository->fetch();
        $this->repository->pull(true);
        $this->runScript();
    }

    private function runScript()
    {
        $command = sprintf('%s/install.sh', $this->path());

        shell_exec($command);
    }

    private function path()
    {
        return home_path($this->path);
    }

    private function url()
    {
        return $this->url();
    }
}
