<?php

namespace App\Console\Commands\Github\Key;

use App\Console\Commands\Github\Token;
use GrahamCampbell\GitHub\Facades\GitHub;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;

class Upload extends Command
{
    protected $signature = 'github:key:upload {--y|yes}';

    protected $description = 'Upload SSH Key to Github';

    public function handle()
    {
        $this->call(Token\Set::class, ['-y' => $this->option('yes')]);

        $this->task('Upload SSH Key to Github', function () {
            $this->upload();
        });
    }

    protected function upload(): void
    {
        if ($this->keyExists()) {
            return;
        }

        Github::me()->keys()->create([
            'title' => $this->hostname(),
            'key' => $this->key(),
        ]);
    }

    protected function keyExists(): bool
    {
        return (bool) $this->keys()->firstWhere('title', $this->hostname());
    }

    protected function hostname(): string
    {
        return gethostname();
    }

    protected function keys(): Collection
    {
        return collect(
            GitHub::me()->keys()->all()
        );
    }

    protected function key(): string
    {
        return File::get(
            home_path('.ssh/id_rsa.pub')
        );
    }
}
