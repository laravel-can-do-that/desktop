<?php

namespace App\Console\Commands\Github\Token;

use Envitor\Facades\DotEnv;
use Illuminate\Console\Command;

class Set extends Command
{
    protected $signature = 'github:token:set {--y|yes}';

    protected $description = 'Set Github personal access token.';

    protected $configPath = 'github.connections.main.token';

    protected $envKey = 'GITHUB_TOKEN';

    public function handle()
    {
        $this->task('Set Github personal access token', function () {
            $token = $this->captureToken();

            $this->persist($token);
        });
    }

    protected function captureToken(): string
    {
        $current = $this->current();

        if ($this->shouldUse($current)) {
            return $current;
        }

        if ($this->hasRequiredScopes($token = $this->prompt())) {
            return $token;
        }

        return $this->captureToken();
    }

    protected function shouldUse($token): bool
    {
        return (bool) $token
            && ($this->option('yes') || $this->confirm("Continue with personal access token ($token)"))
            && $this->hasRequiredScopes($token);
    }

    protected function prompt(): string
    {
        return $this->secret('Personal access token');
    }

    protected function persist(string $token): void
    {
        DotEnv::set($this->envKey, $token);
        config()->set($this->configPath, $token);
    }

    protected function current(): string | null
    {
        return config()->get($this->configPath);
    }

    protected function hasRequiredScopes(string | null $token): bool
    {
        return true;
    }
}
