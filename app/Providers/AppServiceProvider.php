<?php

namespace App\Providers;

use Illuminate\Console\Command;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Command::macro(
            'task',
            function (string $title, $task = null, $loadingText = 'loading...') {
                $this->output->write("$title: <comment>{$loadingText}</comment>");

                if ($task === null) {
                    $result = true;
                } else {
                    try {
                        $result = $task() === false ? false : true;
                    } catch (\Exception $taskException) {
                        $result = false;
                    }
                }

                if ($this->output->isDecorated()) { // Determines if we can use escape sequences
                    // Move the cursor to the beginning of the line
                    $this->output->write("\x0D");

                    // Erase the line
                    $this->output->write("\x1B[2K");
                } else {
                    $this->output->writeln(''); // Make sure we first close the previous line
                }

                $this->output->writeln(
                    "$title: ".($result ? '<info>✔</info>' : '<error>failed</error>')
                );

                if (isset($taskException)) {
                    throw $taskException;
                }

                return $result;
            }
        );
    }
}
