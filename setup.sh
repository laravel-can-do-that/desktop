#!/bin/sh

symlinkConfiguration () {
    [ -d "$HOME/.config/alacritty" ] || ln -s -f "$PWD/dotfiles/.config/alacritty" "$HOME/.config/alacritty"
    [ -d "$HOME/.config/bspwm" ] || ln -s -f "$PWD/dotfiles/.config/bspwm" "$HOME/.config/bspwm"
    [ -d "$HOME/.config/dunst" ] || ln -s -f "$PWD/dotfiles/.config/dunst" "$HOME/.config/dunst"
    [ -d "$HOME/.config/feh" ] || ln -s -f "$PWD/dotfiles/.config/feh" "$HOME/.config/feh"
    [ -d "$HOME/.config/neofetch/themes" ] || ln -s -f "$PWD/dotfiles/.config/neofetch/themes" "$HOME/.config/neofetch/themes"
    [ -d "$HOME/.config/picom" ] || ln -s -f "$PWD/dotfiles/.config/picom" "$HOME/.config/picom"
    [ -d "$HOME/.config/polybar" ] || ln -s -f "$PWD/dotfiles/.config/polybar" "$HOME/.config/polybar"
    [ -d "$HOME/.config/rofi" ] || ln -s -f "$PWD/dotfiles/.config/rofi" "$HOME/.config/rofi"
    [ -d "$HOME/.config/sxhkd" ] || ln -s -f "$PWD/dotfiles/.config/sxhkd" "$HOME/.config/sxhkd"
    [ -d "$HOME/.config/xsettings" ] || ln -s -f "$PWD/dotfiles/.config/xsettings" "$HOME/.config/xsettings"
    [ -f "$HOME/.config/ulauncher/launch.sh" ] || ln -s -f "$PWD/dotfiles/.config/ulauncher/launch.sh" "$HOME/.config/ulauncher/launch.sh"
    [ -f "$HOME/.zshrc" ] || ln -s -f "$PWD/dotfiles/.zshrc" "$HOME/.zshrc"
    [ -f "$HOME/.p10k.zsh" ] || ln -s -f "$PWD/dotfiles/.p10k.zsh" "$HOME/.p10k.zsh"
    [ -f "$HOME/.Xnord" ] || ln -s -f "$PWD/dotfiles/.Xnord" "$HOME/.Xnord"
    [ -f "$HOME/.Xresources" ] || ln -s -f "$PWD/dotfiles/.Xresources" "$HOME/.Xresources"

    ln -sf "$PWD/dotfiles/.config/bpytop/bpytop.conf" "$HOME/.config/bpytop/bpytop.conf"
}

symlinkDesktopLaunchers () {
    [ -f "$HOME/.local/share/applications/code_code.desktop" ] || ln -s -f "$PWD/dotfiles/.local/share/applications/code_code.desktop" "$HOME/.local/share/applications/code_code.desktop"
    [ -f "$HOME/.local/share/applications/com.alacritty.Alacritty.desktop" ] || ln -s -f "$PWD/dotfiles/.local/share/applications/com.alacritty.Alacritty.desktop" "$HOME/.local/share/applications/com.alacritty.Alacritty.desktop"
    [ -f "$HOME/.local/share/applications/debian-uxterm.desktop" ] || ln -s -f "$PWD/dotfiles/.local/share/applications/debian-uxterm.desktop" "$HOME/.local/share/applications/debian-uxterm.desktop"
    [ -f "$HOME/.local/share/applications/debian-xterm.desktop" ] || ln -s -f "$PWD/dotfiles/.local/share/applications/debian-xterm.desktop" "$HOME/.local/share/applications/debian-xterm.desktop"
    [ -f "$HOME/.local/share/applications/mate-screenshot.desktop" ] || ln -s -f "$PWD/dotfiles/.local/share/applications/mate-screenshot.desktop" "$HOME/.local/share/applications/mate-screenshot.desktop"
    [ -f "$HOME/.local/share/applications/menulibre.desktop" ] || ln -s -f "$PWD/dotfiles/.local/share/applications/menulibre.desktop" "$HOME/.local/share/applications/menulibre.desktop"
    [ -f "$HOME/.local/share/applications/org.gnome.Nautilus.desktop" ] || ln -s -f "$PWD/dotfiles/.local/share/applications/org.gnome.Nautilus.desktop" "$HOME/.local/share/applications/org.gnome.Nautilus.desktop"
    [ -f "$HOME/.local/share/applications/org.gnome.Screenshot.desktop" ] || ln -s -f "$PWD/dotfiles/.local/share/applications/org.gnome.Screenshot.desktop" "$HOME/.local/share/applications/org.gnome.Screenshot.desktop"
}

symlinkBackground () {
    [ -f "$HOME/.background.jpg" ] || ln -s -f "$PWD/assets/background.jpg" "$HOME/.background.jpg"
}

symlinkBin () {
    ensureBinDirectoryExists
    [ -f "$HOME/.local/bin/startup" ] || ln -s -f "$PWD/bin/startup.sh" "$HOME/.local/bin/startup"
    [ -f "$HOME/.local/bin/desktop" ] || ln -s -f "$PWD/artisan" "$HOME/.local/bin/desktop"
    [ -f "$HOME/.local/bin/xsettingsd-autostart" ] || ln -s -f "$PWD/bin/xsettingsd-autostart.sh" "$HOME/.local/bin/xsettingsd-autostart"
}

ensureBinDirectoryExists() {
    [ -d "$HOME/.local/bin" ] || mkdir "$HOME/.local/bin"
}

ensureThemesDirectoryExists () {
    [ -d "$HOME/.themes" ] || mkdir "$HOME/.themes"
}

ensureConfigBpytopDirectoryExists () {
    [ -d "$HOME/.config/bpytop" ] || mkdir "$HOME/.config/bpytop"
}

ensureConfigUlauncherDirectoryExists () {
    [ -d "$HOME/.config/ulauncher" ] || mkdir "$HOME/.config/ulauncher"
}

ensureConfigNeofetchDirectoryExists () {
    [ -d "$HOME/.config/neofetch" ] || mkdir "$HOME/.config/neofetch"
}

ensureLocalShareApplicationsDirectoryExists () {
    [ -d "$HOME/.local/share/applications" ] || mkdir "$HOME/.local/share/applications"
}

ensureProgramsDirectoryExists () {
    [ -d "$HOME/.programs/themes" ] || mkdir -p "$HOME/.programs/themes"
    [ -d "$HOME/.programs/themes/gtk" ] || mkdir -p "$HOME/.programs/themes/gtk"
    [ -d "$HOME/.programs/applications" ] || mkdir -p "$HOME/.programs/applications"
}

symlinkBin
symlinkBackground
ensureConfigBpytopDirectoryExists
ensureLocalShareApplicationsDirectoryExists
symlinkDesktopLaunchers
ensureConfigUlauncherDirectoryExists
ensureConfigNeofetchDirectoryExists
symlinkConfiguration
ensureThemesDirectoryExists
ensureProgramsDirectoryExists
